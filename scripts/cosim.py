#!/usr/bin/env python3
import time
import sys
import argparse
import logging

import numpy as np

from Bio import AlignIO

from dendropy import Tree

from libcosim.model import M0, M3, M8, BranchSite
from libcosim.matrix import F0, dirichlet_frequency, read_frequency


def generate_seed():
    return int(time.time() * 256) % 4294967296 # use fractional seconds

def set_leaf_hash_label(tree):
    # if taxon label has #1 after the name, rename and set label
    for node in tree.postorder_node_iter():
        if node.taxon is not None:
            if '#' in node.taxon.label:
                node.taxon.label, lab = node.taxon.label.rsplit('#', 1)
                node.label = '#' + lab
                if node.taxon.label == '':
                    tree.taxon_namespace.remove_taxon(node.taxon)
                    node.taxon = None

def scale_tree(t, multiplier):
    for edge in t.postorder_edge_iter():
        try:
            edge.length *= multiplier
        except TypeError:
            pass
    return t

def comma_float_list(s):
    return [float(v) for v in s.split(',')]


if __name__ == '__main__':
    start_time = time.time()

    parser = argparse.ArgumentParser(
        description="Simulate sequence")

    parser.add_argument('--model', '-m', default='M0',
                        help='simulate under the model (M0, M3, M8, or BS)')

    parser.add_argument('--frequency-alpha', '-f', default=None,
                        type=float,
                        help='Dirichlet-distributed codon '
                        'frequency alpha parameter; by default '
                        'all the frequencies equal to 1/61')

    parser.add_argument('--frequency-file', type=argparse.FileType('r'),
                        help='Codon frequency file with 61 values')

    parser.add_argument('--omega', '-w', default=0.3, type=float,
                        help='omega or omega0 value')

    parser.add_argument('--kappa', '-k', default=2., type=float,
                        help='kappa (transition/transversion) ratio')

    parser.add_argument('--codon-alpha', '-a', default=None, type=float,
                        help='alpha parameter value for codon gamma '
                        'rates distribution')

    parser.add_argument('--codon-ncat', default=0, type=int,
                        help='number of categories for the codon '
                        'gamma rates distribution '
                        '(sample directly from the distribution by default)')

    parser.add_argument('--codon-rates', default=None,
                        type=comma_float_list,
                        help='a comma separated list of codon rates '
                        '(no variation by default)')

    parser.add_argument('--codon-props', default=None,
                        type=comma_float_list,
                        help='a comma separated list of codon rate '
                        'proportions (no variation by default)')

    parser.add_argument('--site-alpha', default=1., type=float,
                        help='alpha parameter value for the site '
                        'gamma rates distribution (for M8 & BS)')

    parser.add_argument('--site-ncat', default=1, type=int,
                        help='number of categories for the site '
                        'gamma rates distribution '
                        '(no variation by default)')

    parser.add_argument('--site-rates', default=None,
                        type=comma_float_list,
                        help='a comma separated list of site rates '
                        '(no variation by default)')

    parser.add_argument('--site-props', default=None,
                        type=comma_float_list,
                        help='a comma separated list of site rate '
                        'proportions (no variation by default)')

    parser.add_argument('--scale-tree', default=None,
                        type=float, help='multiple all branch lengths '
                        'by a factor')

    group_bs = parser.add_argument_group('Branch-site model')

    group_bs.add_argument('--omega2', '-W', default=2., type=float,
                        help='omega2 value for the BS model, '
                          'or it\'s mean value if omega2-ncat > 1; '
                          'omega value for M8 model')

    group_bs.add_argument('--p0', default=0.7, type=float,
                        help='p0 proportion for the BS model or M8 model')

    group_bs.add_argument('--p1', default=0.1, type=float,
                        help='p1 proportion for the BS model')

    group_ebs = parser.add_argument_group('Extended branch-site-model')

    group_ebs.add_argument('--omega3', default=None, type=float,
                        help='omega3 for extended BS: '
                           'background positive selection for #2 branches')

    group_ebs.add_argument('--p3', default=0., type=float,
                        help='proportion for omega3 sites for extended BS')

    group_ebs.add_argument('--omega0-a', default=1., type=float,
                        help='omega0 beta distribution alpha for M8 or '
                          'extended BS model, only works if '
                          'omega0-ncat > 1')

    group_ebs.add_argument('--omega0-b', default=1., type=float,
                        help='omega0 beta distribution beta for M8 or '
                          'extended BS model, only works if '
                          'omega0-ncat > 1')

    group_ebs.add_argument('--omega0-ncat', default=1, type=int,
                        help='number of categories for omega0, '
                          'if equal 1 (default) fixed omega0 '
                           'is used')

    group_ebs.add_argument('--omega2-var', default=1., type=float,
                        help='omega2 variance for the '
                          'extended BS model, only works if '
                          'omega2-ncat > 1')

    group_ebs.add_argument('--omega2-ncat', default=1, type=int,
                        help='number of categories for omega2, '
                          'if equal 1 (defalut) equivalent of'
                          'fixed omega2 value')

    group_m3 = parser.add_argument_group('M3')

    group_m3.add_argument('--omega-list', default=[1.], type=comma_float_list,
                        help='comma separated list of omega values (for M3)')

    group_m3.add_argument('--p-list', default=[], type=comma_float_list,
                        help='comma separated list of proportions (for M3)')

    parser.add_argument('intree', type=argparse.FileType('r'),
                        help='input tree newick file')

    parser.add_argument('alen', type=int,
                        help='alignment length in codons')

    parser.add_argument('alignment', type=argparse.FileType('w'),
                        default='-', nargs='?',
                        help='alignment output file')

    parser.add_argument('--internal', action='store_true',
                        help='export sequences for internal nodes (you need to label them)')

    parser.add_argument('--seed', '-s', type=int,
                        default=generate_seed())

    parser.add_argument('--debug', '-d', action='store_const',
                        const=logging.DEBUG, default=logging.INFO,
                        dest='level', help='enable debug mode')

    parser.add_argument('--log', '-l', type=argparse.FileType('w'),
                        default=sys.stderr,
                        help='save log to file')

    args = parser.parse_args()

    logging.basicConfig(level=args.level, format='%(message)s',
                        stream=args.log)

    tree = Tree.get_from_stream(args.intree, 'newick',
                                suppress_internal_node_taxa=not args.internal)

    set_leaf_hash_label(tree)

    if args.scale_tree is not None:
        logging.info('Scaling tree by %f', args.scale_tree)
        scale_tree(tree, args.scale_tree)

    logging.info('Random seed: %d', args.seed)
    np.random.seed(args.seed)

    logging.info('Model: %s, length=%d', args.model, args.alen)
    if args.codon_alpha is not None:
        logging.info('Codon gamma rates: alpha=%f (ncat=%d)', args.codon_alpha, args.codon_ncat)

    if args.frequency_file:
        logging.info('Codon frequency file: %s',
                     args.frequency_file.name)
        freq = read_frequency(args.frequency_file)
        args.frequency_file.close()
    elif args.frequency_alpha is not None:
        logging.info('Codon frequency alpha=%f',
                     args.frequency_alpha)
        freq = dirichlet_frequency(args.frequency_alpha)
        logging.debug('Codon frequency: %s', freq)
    else:
        freq = F0

    if args.model == 'M0':
        logging.info('omega=%f, kappa=%f',
                     args.omega, args.kappa)
        m = M0(tree, args.alen, args.omega, args.kappa,
               frequency=freq,
               codon_gamma_alpha=args.codon_alpha,
               codon_gamma_ncat=args.codon_ncat)

    elif args.model == 'M3':
        logging.info('omega=%s, p=%s, kappa=%f',
                     args.omega_list, args.p_list, args.kappa)
        m = M3(tree, args.alen, args.p_list, args.omega_list,
               args.kappa, frequency=freq,
               codon_gamma_alpha=args.codon_alpha,
               codon_gamma_ncat=args.codon_ncat)

    elif args.model == 'M8':
        logging.info('omega0 distribution: a=%f, b=%f, ncat=%d, p0=%f',
                     args.omega0_a, args.omega0_b, args.omega0_ncat,
                     args.p0)
        logging.info('omega2=%f', args.omega2)
        logging.info('kappa=%f', args.kappa)
        if args.site_ncat > 1:
            logging.info('site variation: alpha=%f, ncat=%d',
                         args.site_alpha, args.site_ncat)

        m = M8(
            tree=tree,
            n=args.alen,
            p=args.omega0_a,
            q=args.omega0_b,
            p0=args.p0,
            ncat_b=args.omega0_ncat,
            omega=args.omega2,
            kappa=args.kappa,
            frequency=freq,
            site_gamma_alpha=args.site_alpha,
            site_gamma_ncat=args.site_ncat,
            site_rates=args.site_rates,
            site_props=args.site_props,
            codon_gamma_alpha=args.codon_alpha,
            codon_gamma_ncat=args.codon_ncat,
            codon_rates=args.codon_rates,
            codon_props=args.codon_props
        )

    elif args.model == 'BS':
        if args.omega0_ncat == 1:
            logging.info('omega0=%f', args.omega)
        if args.omega2_ncat == 1:
            logging.info('omega2=%f', args.omega2)
        logging.info('kappa=%f', args.kappa)
        logging.info('p0=%f, p1=%f',
                     args.p0, args.p1)
        if args.omega0_ncat > 1:
            logging.info('omega0_a=%f, omega0_b=%f, omega0_ncat=%d',
                         args.omega0_a, args.omega0_b, args.omega0_ncat)
        if args.omega2_ncat > 1:
            logging.info('omega2_mean=%f, omega2_var=%f, omega2_ncat=%d',
                         args.omega2, args.omega2_var, args.omega2_ncat)
        if args.site_ncat > 1:
            logging.info('site variation: alpha=%f, ncat=%d',
                         args.site_alpha, args.site_ncat)
        if args.omega3 is not None:
            logging.info('omega3=%f, p3=%f',
                         args.omega3, args.p3)
        m = BranchSite(
            tree=tree,
            n=args.alen,
            omega0=args.omega,
            omega2=args.omega2,
            p0=args.p0,
            p1=args.p1,
            kappa=args.kappa,
            frequency=freq,
            omega3=args.omega3,
            p3=args.p3,
            omega2_var=args.omega2_var,
            omega2_ncat=args.omega2_ncat,
            omega0_a=args.omega0_a,
            omega0_b=args.omega0_b,
            omega0_ncat=args.omega0_ncat,
            site_gamma_alpha=args.site_alpha,
            site_gamma_ncat=args.site_ncat,
            site_rates=args.site_rates,
            site_props=args.site_props,
            codon_gamma_alpha=args.codon_alpha,
            codon_gamma_ncat=args.codon_ncat,
            codon_rates=args.codon_rates,
            codon_props=args.codon_props)

    else:
        raise ValueError("Unknown model name", args.model)

    m.evolve()
    ext = args.alignment.name.rsplit('.', 1)[-1]
    if ext in ('phy', 'phylip'):
        outfmt = 'phylip-sequential'
    else:
        outfmt = 'fasta'
    AlignIO.write(m.alignment(args.internal), args.alignment, outfmt)
    args.alignment.close()
    logging.info('Running time: %fs', time.time() - start_time)
