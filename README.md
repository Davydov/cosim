Cosim is a codon sequence simulator. It's in an early beta stage.

Cosim supports
[M0](http://mbe.oxfordjournals.org/content/11/5/725.abstract) and
[Branch-Site](http://mbe.oxfordjournals.org/content/22/12/2472 )
models. Implementation of other
[GY94](http://mbe.oxfordjournals.org/content/11/5/725.abstract)-based
models is trivial. Unlike other popular codon sequence simulator,
cosim supports Gamma distributed mutation rates variation.

## Branch-site model extensions

* ω₂ variation: you can specify desired mean, variance for the Gamma
  distribution, approximated with the discrete categories
* ω₀ variation: you can specify α and β values for the Beta
  distribution, approximated with the discrete categories

## Examples

Simulate a sequence alignment (500 codons) with Branch-Site model,
variable ω₂~Gamma(mean=10, var=2), variable ω₀~Beta(α=0.3, β=1) and
κ=2.
```
#!bash
$ cosim.py --model BS --omega2 10 --kappa 2 --omega2-ncat 4 --omega2-var 2 --omega0-a 0.3 --omega0-b 1 --omega0-ncat 3  tree.nwk 500 seq.phy
```

Simulate a sequence alignment (300 codons) with M0 model (ω=0.2, κ=2)
and ~Gamma(1,1) distributed mutation rates.
```
#!bash
$ cosim.py --model M0 --omega 0.2 --kappa 2 --alpha 1 tree.nwk 500 seq.phy
```
