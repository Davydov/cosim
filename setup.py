from setuptools import setup, find_packages

setup(
    name='cosim',
    version='0.1',
    packages=find_packages(),
    scripts=['scripts/cosim.py'],
)
