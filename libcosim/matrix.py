import numpy as np
import logging
from scipy.linalg import eig, eigh, inv, expm

from numpy.random import dirichlet

from .codon import codons, is_transition, is_synonymous
from .codon import distance, substitution_position


F0 = np.array([1./len(codons)] * len(codons))

def dirichlet_frequency(alpha):
    freq = dirichlet([alpha] * len(codons))
    logging.debug('Codon frequency sum: %s', sum(freq))
    return freq

def read_frequency(f):
    freq = []
    for line in f:
        freq.extend(float(v) for v in line.split())
    freq = np.array(freq)
    logging.debug('Codon frequency from file: %s', freq)
    if len(freq) != len(codons):
        logging.warning('Incorrect codon frequency file, '
                        'using F0 instead.')
        return F0
    freq = freq / sum(freq)
    logging.debug('After normalization: %s', freq)
    logging.debug('Codon frequency after normalization: %s', freq)
    logging.debug('Codon frequency sum: %s', sum(freq))
    return freq


class TransitionMatrix(object):
    def __init__(self, omega, kappa, frequency, rates=[1., 1., 1.]):
        # check all the value types
        assert len(rates) == 3

        self.size = len(codons)
        self.omega = float(omega)
        self.kappa = float(kappa)
        if frequency is None:
            frequency = F0
        self.frequency = np.array(frequency, dtype=float)
        assert np.allclose(1, np.sum(self.frequency))
        self.rates = np.array(rates, dtype=float)
        self._create_Q()
        self.V = None
        self.Scratch = np.zeros([self.size, self.size])
        self.P = np.zeros([self.size, self.size])
        self.eig = True

    def _create_Q(self):
        self.Q = np.zeros([self.size, self.size])
        for i, c1 in enumerate(codons):
            row_sum = 0.
            for j, c2 in enumerate(codons):
                if distance(c1, c2) == 1:
                    subs_pos = substitution_position(c1, c2)
                    assert subs_pos >= 0 and subs_pos <= 2
                    self.Q[i, j] = self.rates[subs_pos]
                    self.Q[i, j] *= self.frequency[j]
                    if is_transition(c1, c2):
                        self.Q[i, j] *= self.kappa
                    if not is_synonymous(c1, c2):
                        self.Q[i, j] *= self.omega
                    row_sum += self.Q[i, j]
            self.Q[i, i] = - row_sum
        self._set_scale()

    def _set_scale(self):
        self.scale = 0.
        for i in range(self.size):
            self.scale += -self.frequency[i] * self.Q[i, i]

    def _eigen(self):
        if np.min(self.frequency) < 1e-20:
            logging.debug('One of the frequencies is zero, fallback to expm')
            self.eig = False
            return
        self.S = np.zeros([self.size, self.size])

        Pi = np.zeros([self.size, self.size])
        np.fill_diagonal(Pi, np.sqrt(self.frequency))
        Pi_i = np.zeros([self.size, self.size])
        np.fill_diagonal(Pi_i, np.sqrt(1./self.frequency))
        A = np.dot(np.dot(Pi, self.Q), Pi_i)
        self.d, R = eigh(A)
        self.V = np.dot(Pi_i, R)
        self.V_i = np.dot(R.T, Pi)

    def _exponentiate(self, t, dest=None):
        if self.V is None and self.eig:
            self._eigen()
        if self.eig:
            np.fill_diagonal(self.S, np.exp(t * self.d))
            np.dot(self.V, self.S, self.Scratch)
            dest = np.dot(self.Scratch, self.V_i, dest)
            np.abs(dest, dest)
        else:
            self.Scratch = np.multiply(self.Q, t, self.Scratch)
            dest = expm(self.Scratch)
        return dest

    def transition(self, state, blen, scale=None):
        scale = self.scale if scale is None else scale
        P = self._exponentiate(blen / scale, self.P)
        return P[state,:]

    def __repr__(self):
        return '<transition matrix: w=%f, k=%f, rates=%s, frequency=%s>' % (
            self.omega, self.kappa, repr(self.rates),
            'F0' if (self.frequency == F0).all() else repr(self.frequency))

class MemoizedTransitionMatrix(TransitionMatrix):
    def __init__(self, *args, **kwargs):
        super(MemoizedTransitionMatrix, self).__init__(*args, **kwargs)
        self.P_cache = {}

    def _exponentiate(self, t, dest=None):
        try:
            return self.P_cache[t]
        except KeyError:
            P = super(MemoizedTransitionMatrix,
                      self)._exponentiate(t)
            self.P_cache[t] = P
            return P
