import numpy as np

import logging

from scipy.stats import norm, gamma as gamma_dist, beta
from scipy.special import gamma, gammaincc, betainc


smallDiff = 1e-6


def mygammainc(a, z):
    # gammaincc computes regularized incomplete upper gamma function.
    # don't forget about gamma(a); it's easier to work w/o gamma
    # not to overflow.
    # more info: http://www.johndcook.com/blog/gamma_python/
    return np.where(np.isinf(z), 0., gammaincc(a, z))


def serialize(method):
    def serialized_method(self, all_l, all_u):
        try:
            iter(all_l)
            assert len(all_l) == len(all_u)
            return np.array([method(self, l, u) for l, u in zip(all_l, all_u)])
        except TypeError:
            return method(self, all_l, all_u)

    return serialized_method


def mean_fallback(method):
    def mean_fallback_method(self, l, u):
        try:
            res = method(self, l, u)
            if np.any(np.isnan(res)):
                raise Exception("value is nan")
            assert l <= res and res <= u
            return res
        except Exception as e:
            try:
                self._interval_warning_reported
            except AttributeError:
                logging.warning('Error getting truncated_mean from <%s>: %s',
                                self, str(e))
                logging.warning('Falling back to an average')
                self._interval_warning_reported = True
            return (u + l)/2

    return mean_fallback_method


class Distribution(object):
    def boundaries(self, ncat):
        return self.rv.ppf(np.linspace(0, 1, ncat + 1))

    def discrete(self, ncat):
        boundaries = self.boundaries(ncat)
        if np.any(np.abs(boundaries[1:] - boundaries[:-1]) < smallDiff):
            logging.warn('Intervals are very small')
            logging.debug('Boundaries: %s', boundaries)

        return self.truncated_mean(boundaries[:-1], boundaries[1:])

    def generate(self, n):
        return self.rv.rvs(n)

    def _sim_intervals(self, ncat, n=1000):
        boundaries = self.boundaries(ncat)
        x = self.generate(n)
        count = []
        mean = []
        for l, u in zip(boundaries[:-1], boundaries[1:]):
            in_range = np.logical_and(x >= l, x < u)
            count.append(np.sum(in_range))
            mean.append(np.mean(x[in_range]))
        return count, mean


class Normal(Distribution):
    def __init__(self, mean, sd):
        self.mean = float(mean)
        self.sd = float(sd)
        self.rv = norm(mean, sd)

    @serialize
    @mean_fallback
    def truncated_mean(self, l, u):
        assert np.all(l < u)
        alpha = (l - self.mean)/self.sd
        beta = (u - self.mean)/self.sd
        return self.mean + (
            (norm.pdf(alpha) - norm.pdf(beta)) /
            (norm.cdf(beta) - norm.cdf(alpha))
        ) * self.sd


class Gamma(Distribution):
    def __init__(self, shape, scale):
        self.shape = float(shape)
        self.scale = float(scale)
        self.rv = gamma_dist(shape, scale=scale)

    @serialize
    @mean_fallback
    def truncated_mean(self, l, u):
        # This formula comes from:
        # Okasha, Mahmoud K., and Iyad MA Alqanoo. "Inference on The Doubly Truncated Gamma Distribution For Lifetime Data."
        # http://www.ijmsi.org/Papers/Volume.2.Issue.11/A031101017.pdf
        k = 1.0 / self.scale / (
            mygammainc(self.shape, l / self.scale) -
            mygammainc(self.shape, u / self.scale)
        )
        return k * self.shape * self.scale ** 2 * (
            mygammainc(self.shape + 1, l / self.scale) -
            mygammainc(self.shape + 1, u / self.scale)
        )

class Beta(Distribution):
    def __init__(self, a, b):
        self.a = float(a)
        self.b = float(b)
        self.rv = beta(a, b)

    @serialize
    @mean_fallback
    def truncated_mean(self, l, u):
        # This comes from here:
        # http://math.stackexchange.com/questions/699550
        # NB: betainc is regularized
        return (self.a) / (self.a + self.b) * (
            betainc(1 + self.a, self.b, l)
            - betainc(1 + self.a, self.b, u)) / (
                betainc(self.a, self.b, l)
                - betainc(self.a, self.b, u))


if __name__ == '__main__':
    print('*** Normal ***')
    a = Normal(1, 1)
    print('new discrete:', a.discrete(10))
    count, mean = a._sim_intervals(10)
    print('count=', count)
    print('mean=', mean)
    print('truncated_mean(0.1, 0.2)=', a.truncated_mean(0.1, 0.2))
    print('truncated_mean(0.1, 0.1)=', a.truncated_mean(0.1, 0.1))
    print()

    print('*** Gamma ***')
    g = Gamma(20, 5)
    print('mean=', g.rv.mean())
    print('var=', np.var(g.rv.rvs(1000)))
    print('disc=', g.discrete(5))
    count, mean = g._sim_intervals(5)
    print('cnt=', count)
    print('mean=', mean)
    print('truncated_mean(0.1, 0.2)=', g.truncated_mean(0.1, 0.2))
    print('truncated_mean(0.1, 0.1)=', g.truncated_mean(0.1, 0.1))
    print('truncated_mean(10, 10.005)=', g.truncated_mean(10, 10.005))
    print()

    print('*** Beta ***')
    b = Beta(2, .5)
    print('bound=', b.boundaries(5))
    print('mean=', b.rv.mean())
    print('var=', np.var(b.rv.rvs(1000)))
    print('disc=', b.discrete(10))
    count, mean = b._sim_intervals(10)
    print('cnt=', count)
    print('mean=', mean)
    print('truncated_mean(0.1, 0.2)=', b.truncated_mean(0.1, 0.2))
    print('truncated_mean(0.1, 0.1)=', b.truncated_mean(0.1, 0.1))
