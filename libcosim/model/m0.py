from .base import Model

from .. import matrix


class M0(Model):
    def __init__(self, tree, n, omega=0.1, kappa=2,
                 frequency=matrix.F0,
                 codon_gamma_alpha=None,
                 codon_gamma_ncat=0):
        super(M0, self).__init__(tree, n,
                                 codon_gamma_alpha,
                                 codon_gamma_ncat)
        self.omega = omega
        self.kappa = kappa
        self.frequency = frequency
        self.Q = self.TM(self.omega, self.kappa, self.frequency)
        self._init_matrices()
        self._assign_scales()

    def _init_matrices(self):
        self.matrices = []
        for i in range(self.n):
            self.matrices.append([self.Q] * len(self.nodes))
