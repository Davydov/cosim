import sys
import logging
from itertools import product

import numpy as np

from .base import Model

from .. import matrix
from ..distribution import Beta, Gamma


class M8(Model):
    def __init__(self, tree, n, p=0.1, q=2., p0=1.0, ncat_b=5, omega=2., kappa=2.,
                 frequency=matrix.F0,
                 site_gamma_alpha=1., site_gamma_ncat=1, site_rates=None, site_props=None,
                 codon_gamma_alpha=None, codon_gamma_ncat=0, codon_rates=None, codon_props=None):

        super(M8, self).__init__(tree, n,
                                 codon_gamma_alpha,
                                 codon_gamma_ncat,
                                 codon_rates,
                                 codon_props)
        self.omega = omega
        self.p = p
        self.q = q
        self.p0 = p0
        self.ncat_b = ncat_b
        if ncat_b == 0:
            self.TM = matrix.TransitionMatrix
            logging.info('Using beta rates from continous distribution')
        self.kappa = kappa
        self.frequency = frequency

        self.site_gamma_alpha = site_gamma_alpha
        self.site_gamma_ncat = site_gamma_ncat
        self.site_rates = site_rates
        self.site_props = site_props

        self._create_matrices()
        self._init_matrices()
        self._assign_scales()

    def _create_matrices(self):
        # gamma rates for sites
        if self.site_gamma_ncat > 1:
            assert self.site_rates is None
            logging.debug('site gamma alpha=%f, ncat=%d', self.site_gamma_alpha,
                          self.site_gamma_ncat)
            g = Gamma(self.site_gamma_alpha, 1. / self.site_gamma_alpha)
            self.site_rates = g.discrete(self.site_gamma_ncat)
            self.site_props = np.array([1 / self.site_gamma_ncat] * self.site_gamma_ncat )
            if len(self.site_rates) != self.site_gamma_ncat:
                logging.error('Incorrect number of categories for site rate variation')
                sys.exit(1)
            else:
                logging.debug('site gamma values: %s', self.site_rates)
        elif self.site_rates:
            self.site_rates = np.array(self.site_rates)
            assert np.all(self.site_rates > 0)
            self.site_props = np.array(self.site_props)
            assert np.all(self.site_props > 0)
            self.site_props /= np.sum(self.site_props)
            mean_rate = np.sum(self.site_rates * self.site_props)
            self.site_rates /= mean_rate
            logging.debug('site rates=%s, site props=%s, ncat=%d, mean_rate=%f',
                          self.site_rates,
                          self.site_props,
                          len(self.site_rates),
                          np.sum(self.site_rates * self.site_props))
        else:
            self.site_rates = [1.]
            self.site_props = [1.]

        assert len(self.site_rates) == len(self.site_props)

        # Q0
        assert self.ncat_b > 1 or self.ncat_b == 0
        b = Beta(self.p, self.q)
        logging.debug('omega0 beta p=%f, q=%f', self.p, self.q)
        if self.ncat_b > 1:
            self.omega0 = b.discrete(self.ncat_b)
            logging.debug('omega0 values: %s', self.omega0)
            if len(self.omega0) != self.ncat_b:
                logging.error('Incorrect number of categories for omega0')
                sys.exit(1)
        else:
            self.omega0 = b.generate(self.n)

        if self.ncat_b > 1:
            ## if we use discrete categories, we can precompute matrices
            # inner lists correspond to different omega values
            self.Q0 = [[self.TM(omega0, self.kappa, self.frequency, [r0, r1, r2]) for omega0 in self.omega0]
                       for r0, r1, r2 in product(self.site_rates, repeat=3)]
            self.Q0_prop = [p0 * p1 * p2 for p0, p1, p2 in product(self.site_props, repeat=3)]
            assert np.abs(np.sum(self.Q0_prop) - 1) < 1e-3
            logging.debug("Q0=%s", self.Q0)
            logging.debug("Q0_prop=%s", self.Q0_prop)

        # Q2
        self.Q2 = [self.TM(self.omega, self.kappa, self.frequency, [r0, r1, r2])
                   for r0, r1, r2 in product(self.site_rates, repeat=3)]

    def _init_matrices(self):
        self.matrices = []
        self._assign_classes((self.p0, 1-self.p0))
        effective_site_rates = []
        site_rate_classes = []

        omega_values = []
        for i in range(self.n):
            site_rate_class = np.random.choice(len(self.Q0), p=self.Q0_prop)
            site_rate_classes.append(site_rate_class)

            if self.classes[i] == 0:
                if self.site_gamma_ncat > 1:
                    assert len(self.Q0) == self.site_gamma_ncat**3
                elif not self.site_rates is None:
                    assert len(self.Q0) == len(self.site_rates)**3
                else:
                    assert len(self.Q0) == 1


                # we're in Q0
                if self.ncat_b > 1:
                    m = np.random.choice(self.Q0[site_rate_class])
                else:
                    m = self.TM(self.omega0[i], self.kappa, self.frequency,
                                np.random.choice(self.site_rates, size=3, replace=True, p=self.site_props))
            else:
                # we're in Q2
                m = self.Q2[site_rate_class]
            effective_site_rates.extend(m.rates)
            omega_values.append(m.omega)
            self.matrices.append([m] * len(self.nodes))
        if len(self.Q0) > 1 and site_rate_classes:
            logging.debug("Sites rate classes: %s", site_rate_classes)
        if self.site_gamma_ncat > 1:
            logging.debug("Site rates: %s", effective_site_rates)
        logging.info("Codon omega values: %s",
                     [w for w in omega_values])
        logging.info("Codons with omega2: %s",
                     [i for i, cl in enumerate(self.classes) if cl==1])
