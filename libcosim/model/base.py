import sys
import logging

import numpy as np
import numpy.random

from Bio.Alphabet import generic_dna
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment

from .. import codon
from .. import matrix
from ..distribution import Gamma

class Model(object):
    def __init__(self, tree, n, codon_alpha, codon_gamma_ncat=0,
                 codon_rates=None, codon_props=None):
        self.ancestral = None
        self.matrices = None
        self.n = n
        self.tree = tree
        self.codon_alpha = codon_alpha
        if codon_alpha is None:
            logging.debug('Using MemoizedTransitionMatrix')
            self.TM = matrix.MemoizedTransitionMatrix
        else:
            self.TM = matrix.TransitionMatrix
        self.codon_gamma_ncat = codon_gamma_ncat
        self.codon_rates = codon_rates
        self.codon_props = codon_props
        self._number_nodes()
        self._init_rates()

    def _init_ancestral(self):
        self.ancestral = []
        for i in range(self.n):
            anc = np.random.choice(len(codon.codons), p=self.frequency)
            self.ancestral.append(anc)
        assert len(self.ancestral) == self.n
        self.tree.seed_node.state = self.ancestral

    def _init_rates(self):
        if self.codon_rates:
            codon_rates = np.array(self.codon_rates)
            assert np.all(codon_rates > 0)
            codon_props = np.array(self.codon_props)
            assert np.all(codon_props > 0)
            codon_props /= np.sum(codon_props)
            assert len(codon_rates) == len(codon_props)
            mean_rate = np.sum(codon_rates * codon_props)
            codon_rates /= mean_rate
            logging.debug('codon rates: %s, codon_props: %s',
                          codon_rates, codon_props)
            logging.debug('codon mean rate: %s',
                          np.sum(codon_rates * codon_props))
            self.rates = np.random.choice(codon_rates, size=self.n,
                                          replace=True, p=codon_props)
            assert self.codon_alpha is None
        elif self.codon_alpha is not None:
            g = Gamma(self.codon_alpha,
                      scale=1./self.codon_alpha)
            if self.codon_gamma_ncat <= 1:
                logging.debug("Sampling codon rates from gamma distribution")
                self.rates = g.generate(self.n)
                assert not self.codon_rates and not self.codon_props
            else:
                codon_rates = g.discrete(self.codon_gamma_ncat)
                if len(codon_rates) != self.codon_gamma_ncat:
                    logging.error('Incorrect number of categories for site rate variation')
                    sys.exit(1)
                logging.debug('codon gamma categories: %s', codon_rates)
                self.rates = np.random.choice(codon_rates, size=self.n, replace=True)
        else:
            self.rates = [1.] * self.n

        if self.codon_rates or self.codon_alpha is not None:
            logging.debug("Codon rates: %s" , self.rates)
            logging.debug("Mean codon rate: %s",
                          np.mean(self.rates))

    def _number_nodes(self):
        self.nodes = []
        for i, node in enumerate(self.tree.preorder_node_iter()):
            self.nodes.append(node)
            node.index = i

    def _assign_scales(self):
        self.scales = []
        for n in range(len(self.nodes)):
            scale = 0.0
            for i in range(self.n):
                scale += self.matrices[i][n].scale * self.rates[i]
            self.scales.append(scale / self.n)

    def _assign_classes(self, prop):
        self.classes = []
        for cl, p in enumerate(prop):
            self.classes.extend([cl] * int(round(p * self.n)))
        ## self.classes can be > n or < n
        ## we first add necessary elements
        if len(self.classes) < self.n:
            logging.debug('Extending classes (%d->%d)' %
                          (len(self.classes), self.n))
            self.classes.extend(np.random.choice(len(prop),
                                                 size=self.n-len(self.classes),
                                                 p=prop))
        np.random.shuffle(self.classes)
        if len(self.classes) > self.n:
            logging.debug('Shrinking classes (%d->%d)' %
                          (len(self.classes), self.n))

        self.classes = self.classes[:self.n]
        logging.debug('Site classes: %s' % self.classes)
        assert len(self.classes) == self.n

    def evolve(self):
        if self.ancestral is None:
            self._init_ancestral()
        if self.matrices is None:
            self._init_matrices()
        for node in self.tree.preorder_node_iter():
            if node.parent_node is None:
                anc = self.ancestral
            else:
                anc = node.parent_node.state
            if node.edge_length is None:
                node.state = anc
                continue
            else:
                node.state = []
            sub = 0
            for i in range(self.n):
                p = self.matrices[i][node.index].transition(anc[i], self.rates[i] * node.edge_length, self.scales[node.index])
                nps = np.sum(p)
                if np.abs(nps - 1) > 0.05:
                    logging.warn('transition probabilities sum to %f, not 1', nps)
                node.state.append(np.random.choice(len(codon.codons), p=p/nps))
                if anc[i] != node.state[-1]:
                    sub += 1
            logging.debug("Branch length: %f, normalized substitutions: %f",
                          node.edge_length, float(sub)/self.n)

    def _state_to_seq(self, state):
        return Seq(''.join(
            (codon.codons[c] for c in state)
        ), generic_dna)

    def alignment(self, internal_nodes=False):
        seqrecs = []
        if internal_nodes == False:
            nodes_iter = self.tree.leaf_node_iter()
        else:
            nodes_iter = self.tree.postorder_node_iter()
        skipped = 0
        for node in nodes_iter:
            if node.taxon is None:
                skipped += 1
                continue
            seqrecs.append(
                SeqRecord(self._state_to_seq(node.state),
                    id=node.taxon.label, description='')
            )
        if skipped > 0:
            logging.warning("skipped %d nodes without taxa labels"  % skipped)
        return MultipleSeqAlignment(seqrecs)
