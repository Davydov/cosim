import sys
import logging
from itertools import product

import numpy as np

from .base import Model

from .. import matrix
from ..distribution import Gamma


class M3(Model):
    def __init__(self, tree, n, p=[],  omega=[1.], kappa=2.,
                 frequency=matrix.F0, site_gamma_alpha=1., site_gamma_ncat=1,
                 codon_gamma_alpha=None, codon_gamma_ncat=0):
        super(M3, self).__init__(tree, n,
                                 codon_gamma_alpha,
                                 codon_gamma_ncat)
        assert np.sum(p) < 1
        assert len(p) + 1 == len(omega)
        self.p = list(p) + [1 - np.sum(p)]
        self.omega = omega
        self.site_gamma_alpha = site_gamma_alpha
        self.site_gamma_ncat = site_gamma_ncat
        self.kappa = kappa
        self.frequency = frequency

        self._create_matrices()
        self._init_matrices()
        self._assign_scales()

    def _create_matrices(self):
        # gamma rates for sites
        if self.site_gamma_ncat > 1:
            logging.debug('site gamma alpha=%f, ncat=%d', self.site_gamma_alpha,
                          self.site_gamma_ncat)
            g = Gamma(self.site_gamma_alpha, 1. / self.site_gamma_alpha)
            self.site_rates = g.discrete(self.site_gamma_ncat)
            if len(self.site_rates) != self.site_gamma_ncat:
                logging.error('Incorrect number of categories for site rate variation')
                sys.exit(1)
            else:
                logging.debug('site gamma values: %s', self.site_rates)
        else:
            self.site_rates = [1.]

        self.Q = [[self.TM(omega, self.kappa, self.frequency, [r0, r1, r2])
                  for r0, r1, r2 in product(self.site_rates, repeat=3)]
                  for omega in self.omega]

    def _init_matrices(self):
        self.matrices = []
        self._assign_classes(self.p)
        site_rates = []
        omega_values = []
        for i in range(self.n):
            m = np.random.choice(self.Q[self.classes[i]])
            site_rates.extend(m.rates)
            omega_values.append(m.omega)
            self.matrices.append([m] * len(self.nodes))
        if self.site_gamma_ncat > 1:
            logging.debug("Site rates: %s", site_rates)
        logging.info("Codon omega values: %s",
                     [w for w in omega_values])
