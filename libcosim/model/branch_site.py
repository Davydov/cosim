import sys
import logging
from itertools import product
from collections import Counter, defaultdict

import numpy as np

from .base import Model

from .. import matrix
from ..distribution import Beta, Gamma


class BranchSite(Model):
    def __init__(self, tree, n, omega0=0.1, omega2=30, p0=0.7, p1=0.1, kappa=2., frequency=matrix.F0,
                 omega3=None, p3=0., omega2_var=3., omega2_ncat=1, omega0_a=1., omega0_b=1., omega0_ncat=1,
                 site_gamma_alpha=1., site_gamma_ncat=1, site_rates=None, site_props=None,
                 codon_gamma_alpha=None, codon_gamma_ncat=0, codon_rates=None, codon_props=None):
        super(BranchSite, self).__init__(tree, n,
                                         codon_gamma_alpha,
                                         codon_gamma_ncat,
                                         codon_rates,
                                         codon_props)
        self.omega0 = [omega0]
        self.omega2_mean = omega2
        self.kappa = kappa
        self.frequency = frequency
        self.p0 = p0
        self.p1 = p1
        self.omega3 = omega3
        self.p3 = p3
        self.omega2_var = omega2_var
        self.omega2_ncat = omega2_ncat
        self.omega0_a = omega0_a
        self.omega0_b = omega0_b
        self.omega0_ncat = omega0_ncat

        self.site_gamma_alpha = site_gamma_alpha
        self.site_gamma_ncat = site_gamma_ncat
        self.site_rates = site_rates
        self.site_props = site_props

        self._create_matrices()

        p2 = 1. - self.p0 - self.p1
        assert p2 >= 0
        self.p2a = p2 * self.p0 / (self.p0 + self.p1)
        self.p2b = p2 * self.p1 / (self.p0 + self.p1)
        logging.debug('p2a=%f, p2b=%f', self.p2a, self.p2b)
        self.frequency = frequency
        self._init_matrices()
        self._assign_scales()

    def _create_matrices(self):
        # gamma rates for sites
        if self.site_gamma_ncat > 1:
            assert self.site_rates is None
            logging.debug('site gamma alpha=%f, ncat=%d', self.site_gamma_alpha,
                          self.site_gamma_ncat)
            g = Gamma(self.site_gamma_alpha, 1. / self.site_gamma_alpha)
            site_rates = g.discrete(self.site_gamma_ncat)
            site_props = np.array([1 / self.site_gamma_ncat] * self.site_gamma_ncat )
            if len(site_rates) != self.site_gamma_ncat:
                logging.error('Incorrect number of categories for site rate variation')
                sys.exit(1)
            else:
                logging.debug('site gamma values: %s', site_rates)
        elif self.site_rates:
            site_rates = np.array(self.site_rates)
            assert np.all(site_rates > 0)
            site_props = np.array(self.site_props)
            assert np.all(site_props > 0)
            site_props /= np.sum(site_props)
            mean_rate = np.sum(site_rates * site_props)
            site_rates /= mean_rate
            logging.debug('site rates=%s, site props=%s, ncat=%d, mean_rate=%f',
                          site_rates,
                          site_props,
                          len(site_rates),
                          np.sum(site_rates * site_props))
        else:
            site_rates = [1.]
            site_props = [1.]

        assert len(site_rates) == len(site_props)

        # Q0
        if self.omega0_ncat > 1:
            b = Beta(self.omega0_a, self.omega0_b)
            logging.debug('omega0 beta a=%f, b=%f', self.omega0_a, self.omega0_b)
            self.omega0 = b.discrete(self.omega0_ncat)
            logging.debug('omega0 values: %s', self.omega0)
            if len(self.omega0) != self.omega0_ncat:
                logging.error('Incorrect number of categories for omega0')
                sys.exit(1)

        # every matrix is a list of lists, outer list correspond to rates
        # inner lists correspond to different omega values
        self.Q0 = [[self.TM(omega0, self.kappa, self.frequency, [r0, r1, r2]) for omega0 in self.omega0]
                   for r0, r1, r2 in product(site_rates, repeat=3)]
        self.Q0_prop = [p0 * p1 * p2 for p0, p1, p2 in product(site_props, repeat=3)]
        assert np.abs(np.sum(self.Q0_prop) - 1) < 1e-3
        logging.debug("Q0=%s", self.Q0)
        logging.debug("Q0_prop=%s", self.Q0_prop)


        # Q1
        self.Q1 = [[self.TM(1, self.kappa, self.frequency, [r0, r1, r2])]
                   for r0, r1, r2 in product(site_rates, repeat=3)]
        logging.debug("Q1=%s", str(self.Q1))

        # Q2
        if self.omega2_ncat > 1:
            # compute gamma distribution parameters, to satisfy sd and mean
            g_scale = self.omega2_var / self.omega2_mean
            g_shape = self.omega2_mean / g_scale
            logging.debug('omega2 gamma shape=%f, scale=%f', g_shape, g_scale)
            g = Gamma(g_shape, g_scale)
            self.omega2 = g.discrete(self.omega2_ncat)
            logging.debug('omega2 values: %s', self.omega2)
            logging.debug('omega2 gamma dist mean=%f, var=%f', g.rv.mean(), g.rv.var())
            if len(self.omega2) != self.omega2_ncat:
                logging.error('Incorrect number of categories for omega2')
                sys.exit(1)
        else:
            self.omega2 = [self.omega2_mean]

        self.Q2 = [[self.TM(omega2, self.kappa, self.frequency, [r0, r1, r2]) for omega2 in self.omega2]
                   for r0, r1, r2 in product(site_rates, repeat=3)]
        logging.debug("Q2=%s", str(self.Q2))

        if self.omega3 is not None:
            self.Q3 = [self.TM(self.omega3, self.kappa, self.frequency, [r0, r1, r2])
                       for r0, r1, r2 in product(site_rates, repeat=3)]
            logging.debug("Q3=%s", str(self.Q3))
        else:
            self.Q3 = None

        if self.site_gamma_ncat > 1:
            assert len(self.Q0) == len(self.Q1) == len(self.Q2) == self.site_gamma_ncat**3
        elif self.site_rates:
            assert len(self.Q0) == len(self.Q1) == len(self.Q2) == len(self.site_rates)**3
        logging.debug('Number of rates for matricies: %d', len(self.Q0))
        logging.debug('Matrix counts: Q0: %d, Q1: %d, Q2: %d', len(self.Q0[0]),
                      len(self.Q1[0]), len(self.Q2[0]))

    def _init_matrices(self):
        self.matrices = []
        self._assign_classes((self.p0, self.p1, self.p2a, self.p2b))
        # we do not use site classes literaly if omega3 is defined
        # for every bg branch there is probability p3 of having Q3
        self.q2classes = Counter()
        site_rate_classes = []
        for i in range(self.n):
            if self.site_gamma_ncat > 1:
                assert len(self.Q0) == self.site_gamma_ncat**3
            elif self.site_rates:
                assert len(self.Q0) == len(self.site_rates)**3
            else:
                assert len(self.Q0) == 1
            site_rate_class = np.random.choice(len(self.Q0), p=self.Q0_prop)
            site_rate_classes.append(site_rate_class)
            site_class = self.classes[i]
            assert site_class < 4
            if site_class == 0:
                bgQ = np.random.choice(self.Q0[site_rate_class])
                fgQ = bgQ
            elif site_class == 1:
                # Normally there should be only single Q1
                assert len(self.Q1[site_rate_class]) == 1
                bgQ = np.random.choice(self.Q1[site_rate_class])
                fgQ = bgQ
            elif site_class == 2:
                bgQ = np.random.choice(self.Q0[site_rate_class])
                q2_class = np.random.choice(len(self.Q2[site_rate_class]))
                self.q2classes[q2_class] += 1
                fgQ = self.Q2[site_rate_class][q2_class]
            elif site_class == 3:
                # Normally there should be only single Q1
                assert len(self.Q1[site_rate_class]) == 1
                bgQ = np.random.choice(self.Q1[site_rate_class])
                q2_class = np.random.choice(len(self.Q2[site_rate_class]))
                self.q2classes[q2_class] += 1
                fgQ = self.Q2[site_rate_class][q2_class]
            brmat = []
            for node in self.nodes:
                if node.label == '#1':
                    brmat.append(fgQ)
                elif self.Q3 is not None and node.label == '#2' and np.random.choice([False, True], p=[1-self.p3, self.p3]):
                    brmat.append(self.Q3[site_rate_class])
                else:
                    brmat.append(bgQ)
            self.matrices.append(brmat)
        if len(self.Q0) > 1:
            logging.debug("Sites rate classes: %s", site_rate_classes)
            for i in range(3):
                logging.debug("Average codon position 1 rate: %s",
                              np.mean([self.Q0[sr][0].rates[i] for sr in site_rate_classes]))
        logging.info("Sites with omega2: %s",
                     [i for i, cl in enumerate(self.classes) if cl in (2,3)])
